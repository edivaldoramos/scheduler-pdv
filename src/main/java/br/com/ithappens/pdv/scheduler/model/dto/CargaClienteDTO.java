package br.com.ithappens.pdv.scheduler.model.dto;

import lombok.Getter;
import lombok.Setter;

public class CargaClienteDTO {

    @Getter
    @Setter
    private Long id;

    @Setter
    private String razaoSocial;

    @Setter
    private String nomeFantasia;

    @Getter
    @Setter
    private String tipoPessoa;

    @Getter
    @Setter
    private String cpfOuCnpj;

    public String getRazaoSocial() {
        return razaoSocial.trim();
    }

    public String getNomeFantasia() {
        return nomeFantasia.trim();
    }
}
