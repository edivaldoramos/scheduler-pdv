package br.com.ithappens.pdv.scheduler.service;

import br.com.ithappens.pdv.scheduler.model.Carga;
import br.com.ithappens.pdv.scheduler.model.dto.CargaClienteDTO;
import org.springframework.stereotype.Component;

import javax.validation.ValidationException;
import java.time.LocalDate;
import java.time.LocalDateTime;
import java.util.List;

@Component
public class ValidacaoGeracaoCargaClienteService implements IValidacaoGeracaoCargaClienteService {
    @Override
    public void validarHashRetornoEnvioServidorRemoto(String hash) {
        if (hash.isEmpty()) {
            throw new ValidationException("Hash inválido.");
        }
    }

    @Override
    public void validarDataInicioProcesso(LocalDateTime dataInicioProcesso) {
        if(dataInicioProcesso.isAfter(LocalDateTime.now())){
            throw new ValidationException("Data de inicio do processo é maior que a data atual.");
        }

        if(dataInicioProcesso.isEqual(LocalDateTime.now())){
            throw new ValidationException("Data de inicio do processo é igual a data atual.");
        }
    }

    @Override
    public void validarUltimaCargaSolicitada(Carga ultimaCargaSolicitada) {
        if (ultimaCargaSolicitada == null) {
            throw new ValidationException("Não existe solicitação de geração de carga.");
        }
    }

    @Override
    public void validarCargaClientesDTOsParaImpostar(List<CargaClienteDTO> cargaClienteDTOs) throws ValidationException {
        if (cargaClienteDTOs.isEmpty()) {
            throw new ValidationException("Não existem clientes novos ou com alterações para importar.");
        }
    }
}
