package br.com.ithappens.pdv.scheduler.model.enums;

import java.util.EnumSet;
import java.util.HashMap;
import java.util.Map;

public enum StatusPontoVenda {

    ABERTO (1), DISPONIVEL (2), PAUSADO (3), FECHADO_PARCIAL (4), FECHADO (5), VENDENDO (6);

    private static Map<Integer, StatusPontoVenda> lookup;

    static {
        lookup = new HashMap<Integer, StatusPontoVenda>();
        EnumSet<StatusPontoVenda> enumSet = EnumSet.allOf(StatusPontoVenda.class);
        for (StatusPontoVenda StatusPontoVenda : enumSet) {
            lookup.put(StatusPontoVenda.codigo, StatusPontoVenda);
        }
    }

    public int codigo;

    private StatusPontoVenda(int codigo) {
        this.codigo = codigo;

    }

    public static StatusPontoVenda fromCodigo(int codigo) {
        if (lookup.containsKey(codigo)) {
            return lookup.get(codigo);
        }
        throw new IllegalArgumentException(String.format("Código do StatusPontoVenda inválido: %d", codigo));
    }

    public int getCodigo() {
        return codigo;
    }

}
