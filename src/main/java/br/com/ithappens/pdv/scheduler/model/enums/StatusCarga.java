package br.com.ithappens.pdv.scheduler.model.enums;

import java.util.EnumSet;
import java.util.HashMap;
import java.util.Map;

public enum StatusCarga {

    GERADA(1), LIBERADA(2), PROCESSANDO(3), PROCESSADA(4), SOLICITADA(5);

    private static Map<Integer, StatusCarga> lookup;

    static {
        lookup = new HashMap<Integer, StatusCarga>();
        EnumSet<StatusCarga> enumSet = EnumSet.allOf(StatusCarga.class);
        for (StatusCarga StatusCarga : enumSet) {
            lookup.put(StatusCarga.codigo, StatusCarga);
        }
    }

    public int codigo;

    private StatusCarga(int codigo) {
        this.codigo = codigo;

    }

    public static StatusCarga fromCodigo(int codigo) {
        if (lookup.containsKey(codigo)) {
            return lookup.get(codigo);
        }
        throw new IllegalArgumentException(String.format("Código do StatusCarga inválido: %d", codigo));
    }

    public int getCodigo() {
        return codigo;
    }
}
