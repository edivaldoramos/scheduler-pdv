package br.com.ithappens.pdv.scheduler.model;

import br.com.ithappens.pdv.scheduler.model.enums.StatusCarga;
import br.com.ithappens.pdv.scheduler.model.enums.TipoCarga;
import lombok.Getter;
import lombok.Setter;

import java.time.LocalDateTime;

public class Carga {

    @Getter
    @Setter
    private Long id;

    @Getter
    @Setter
    private TipoCarga tipoCarga;

    @Getter
    @Setter
    private String tipoPessoa;

    private Filial filial;

    private PontoVenda pontoVenda;

    @Getter
    @Setter
    private LocalDateTime data;

    @Getter
    @Setter
    private StatusCarga status;

    @Getter
    private String hash;

    public void setHash(String hash) {
        this.hash = hash;
        this.setStatus(StatusCarga.PROCESSANDO);
    }

    public Filial getFilial() {
        return filial;
    }

    public void setFilial(Filial filial) {
        this.filial = filial;
    }

    public PontoVenda getPontoVenda() {
        return pontoVenda;
    }

    public void setPontoVenda(PontoVenda pontoVenda) {
        this.pontoVenda = pontoVenda;
    }

    public LocalDateTime getDataRecuperarClientesParaGerarCarga(Carga ultimaCargaConcluida) {
        LocalDateTime dataRecuperarClientesParaGerarCarga = null;

        if (ultimaCargaConcluida == null) {
            dataRecuperarClientesParaGerarCarga = LocalDateTime.of(1900, 01, 01, 00, 00, 00);
        } else {
            dataRecuperarClientesParaGerarCarga = ultimaCargaConcluida.getData();
        }

        return dataRecuperarClientesParaGerarCarga;
    }
}
