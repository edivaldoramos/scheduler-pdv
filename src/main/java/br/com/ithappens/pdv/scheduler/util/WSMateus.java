package br.com.ithappens.pdv.scheduler.util;

import br.com.ithappens.pdv.scheduler.model.dto.wsmateus.FileData;
import br.com.ithappens.pdv.scheduler.service.GeracaoCargaCliente;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.cache.annotation.Cacheable;
import org.springframework.core.io.ByteArrayResource;
import org.springframework.http.*;
import org.springframework.stereotype.Component;
import org.springframework.util.LinkedMultiValueMap;
import org.springframework.util.MultiValueMap;
import org.springframework.web.client.RestTemplate;

import java.io.IOException;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.Optional;

@Component
public class WSMateus implements IWSMateus {

    private final static String URL_DOWNLOAD = "http://webserver.mateus:8080/download";
    private final static String URL_UPLOAD = "http://webserver.mateus:8080/upload";
    private final static SimpleDateFormat SDF = new SimpleDateFormat("yyyy-MM-dd HH:mm:ss");

    @Autowired
    private GeracaoCargaCliente geracaoCargaCliente;

    @Autowired
    private RestTemplate restTemplate;

    @Override
    public FileData download(String hash) throws IOException {
        return download(hash, null);
    }

    @Override
    @Cacheable(sync = true)
    public FileData download(String hash, String senha) throws IOException {
        ResponseEntity<byte[]> exchange = get(hash, Optional.ofNullable(senha).orElse(""), false);
        if (exchange.getStatusCodeValue() == 200) {
            FileData fileData = new FileData();
            fileData.setCategoria(exchange.getHeaders().getFirst("ws-categoria"));
            fileData.setNomeDoArquivo(exchange.getHeaders().getFirst("ws-arquivo"));
            fileData.setObservacoes(exchange.getHeaders().getFirst("ws-observacao"));
            fileData.setUrlDownload(exchange.getHeaders().getFirst("ws-urldownload"));
            try {
                fileData.setArmazenadoEm(SDF.parse(exchange.getHeaders().getFirst("ws-datahora")));
            } catch (ParseException e) {
                fileData.setArmazenadoEm(null);
            }
            fileData.setConteudo(exchange.getBody());
            fileData.setSucesso(true);
            return fileData;
        } else {
            throw new IOException("Erro: download de hash " + hash + " retornou status " + exchange.getStatusCodeValue());
        }
    }

    @Override
    public FileData metadados(String hash) throws IOException {
        return metadados(hash, null);
    }

    @Override
    public FileData metadados(String hash, String senha) throws IOException {
        ResponseEntity<byte[]> exchange = get(hash, Optional.ofNullable(senha).orElse(""), true);
        if (exchange.getStatusCodeValue() == 200) {
            FileData fileData = new FileData();
            fileData.setCategoria(exchange.getHeaders().getFirst("ws-categoria"));
            fileData.setNomeDoArquivo(exchange.getHeaders().getFirst("ws-arquivo"));
            fileData.setObservacoes(exchange.getHeaders().getFirst("ws-observacao"));
            fileData.setUrlDownload(exchange.getHeaders().getFirst("ws-urldownload"));
            try {
                fileData.setArmazenadoEm(SDF.parse(exchange.getHeaders().getFirst("ws-datahora")));
            } catch (ParseException e) {
                fileData.setArmazenadoEm(null);
            }
            fileData.setSucesso(true);
            return fileData;
        } else {
            throw new IOException("Erro: download de hash " + hash + " retornou status " + exchange.getStatusCodeValue());
        }
    }

    @Override
    public String upload(String categoria, byte[] dados, String nomeDoArquivo) throws IOException {
        return upload(categoria, dados, nomeDoArquivo, null, null);
    }

    @Override
    public String upload(String categoria, byte[] dados, String nomeDoArquivo, String observacoes) throws IOException {
        return upload(categoria, dados, nomeDoArquivo, observacoes, null);
    }

    @Override
    public String upload(String categoria, byte[] dados, String nomeDoArquivo, String observacoes, String senha) throws IOException {
        ResponseEntity<byte[]> exchange = post(categoria, dados, nomeDoArquivo, observacoes, senha);
        return exchange.getHeaders().getFirst("ws-arquivo-hash");
    }

    private ResponseEntity<byte[]> get(String hash, String senha, boolean somenteMetadata) {
        HttpHeaders headers = new HttpHeaders();
        headers.add("ws-senha", senha);
        if (somenteMetadata)
            headers.add("ws-metadataonly", "true");
        HttpEntity<String> entity = new HttpEntity<String>("parameters", headers);
        ResponseEntity<byte[]> exchange = restTemplate.exchange(URL_DOWNLOAD + "?hash=" + hash, HttpMethod.GET, entity, byte[].class);
        return exchange;
    }

    private ResponseEntity<byte[]> post(String categoria, byte[] dados, String nomeDoArquivo, String observacoes, String senha) throws IOException {
        HttpHeaders headers = new HttpHeaders();
        headers.add("ws-senha", Optional.ofNullable(senha).orElse(""));
        headers.add("ws-categoria", categoria);
        headers.add("ws-observacao", Optional.ofNullable(observacoes).orElse(""));
        headers.add("ws-arquivo", nomeDoArquivo);
        headers.setContentType(MediaType.MULTIPART_FORM_DATA);

        MultiValueMap<String, Object> multiPart = new LinkedMultiValueMap<>();
        HttpHeaders subHeaders = new HttpHeaders();
        subHeaders.setContentType(MediaType.APPLICATION_OCTET_STREAM);
        subHeaders.setContentDispositionFormData("data", "data");
        HttpEntity<ByteArrayResource> bodyPart = new HttpEntity<>(new ByteArrayResource(dados), subHeaders);

        multiPart.add("data", bodyPart);

        HttpEntity<MultiValueMap<String, Object>> entity = new HttpEntity<MultiValueMap<String, Object>>(multiPart, headers);
        ResponseEntity<byte[]> exchange = restTemplate.exchange(URL_UPLOAD, HttpMethod.POST, entity, byte[].class);

        if (!exchange.getHeaders().containsKey("ws-arquivo-hash"))
            throw new IOException("Erro no upload:" + exchange.getStatusCode().getReasonPhrase());
        return exchange;
    }

}
