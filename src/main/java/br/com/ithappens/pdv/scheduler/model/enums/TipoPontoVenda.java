package br.com.ithappens.pdv.scheduler.model.enums;

public enum TipoPontoVenda {

    PDV (1, "Pdv"),
    SERVIDOR (2, "Servidor");

    private int codigo;
    private String descricao;

    TipoPontoVenda(int codigo, String descricao) {
        this.codigo = codigo;
        this.descricao = descricao;
    }

    public int getCodigo() {
        return this.codigo;
    }

    public String getDescricao() {
        return this.descricao;
    }

    public static TipoPontoVenda valueOfCodigo(Integer codigo) {
        if (codigo == null) {
            return null;
        } else {
            TipoPontoVenda[] var4;
            int var3 = (var4 = values()).length;

            for(int var2 = 0; var2 < var3; ++var2) {
                TipoPontoVenda tipoPontoVenda = var4[var2];
                if (tipoPontoVenda.getCodigo() == codigo.intValue()) {
                    return tipoPontoVenda;
                }
            }
            return null;
        }
    }

}
