package br.com.ithappens.pdv.scheduler.util;

import br.com.ithappens.pdv.scheduler.model.dto.wsmateus.FileData;

import java.io.IOException;

public interface IWSMateus {

    FileData download(String hash) throws IOException;

    FileData download(String hash, String senha) throws IOException;

    FileData metadados(String hash) throws IOException;

    FileData metadados(String hash, String senha) throws IOException;

    String upload(String categoria, byte[] dados, String nomeDoArquivo) throws IOException;

    String upload(String categoria, byte[] dados, String nomeDoArquivo, String observacoes) throws IOException;

    String upload(String categoria, byte[] dados, String nomeDoArquivo, String observacoes, String senha) throws IOException;

}
