package br.com.ithappens.pdv.scheduler.mapper;

import br.com.ithappens.pdv.scheduler.model.Carga;
import br.com.ithappens.pdv.scheduler.model.enums.TipoCarga;
import org.apache.ibatis.annotations.Mapper;
import org.apache.ibatis.annotations.Param;
import org.springframework.stereotype.Repository;

import java.time.LocalDateTime;

@Mapper
@Repository
public interface CargaMapper {

    Boolean atualizar(@Param("carga") Carga carga);

    Boolean atualizarCarga(@Param("hash") String hash, @Param("dataCarga") LocalDateTime dataCarga);

    Carga recuperarUltimaCargaProcessada(@Param("tipoCarga") TipoCarga tipoCarga);

    Carga recuperarUltimaCargaSolicitada(@Param("tipoCarga") TipoCarga tipoCarga);

    Carga recuperarCargaPorHashEData(@Param("hash") String hash, @Param("dataCarga") LocalDateTime dataCarga);

}
