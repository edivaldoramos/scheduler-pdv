package br.com.ithappens.pdv.scheduler.service;

import org.springframework.stereotype.Component;

import java.io.IOException;
import java.time.LocalDateTime;

@Component
public abstract class GeradorArquivoCarga {

    /**
     * Método gera carga de clientes
     *
     * @author Edivaldo Ramos
     */
    public void gerarCarga() throws IOException {
        LocalDateTime dataInicioProcesso = LocalDateTime.now();
        this.gerarArquivo();
        this.compactarArquivo();
        String hash = this.getHashArquivoEnviadoServidorRemoto();
        this.vincularHashArquivoAoControlePdv(hash, dataInicioProcesso);
    }

    abstract void gerarArquivo();

    abstract void compactarArquivo() throws IOException;

    abstract String getHashArquivoEnviadoServidorRemoto() throws IOException;

    abstract void vincularHashArquivoAoControlePdv(String hash, LocalDateTime dataInicioProcesso);

    abstract String getNomeArquivo();

    String getCaminhoCompletoArquivoPlanilha() {
        return this.getCaminhoPastaPadraoUsuario() + "/" + this.getNomeArquivoPlanilha();
    }

    String getCaminhoCompletoArquivoCompactado() {
        return this.getCaminhoPastaPadraoUsuario() + "/" + this.getNomeArquivoCompactado();
    }

    String getNomeArquivoCompactado() {
        return this.getNomeArquivo() + ".7z";
    }

    String getNomeArquivoPlanilha() {
        return this.getNomeArquivo() + ".csv";
    }

    String getCaminhoPastaPadraoUsuario() {
        return System.getProperty("user.home");
    }

}
