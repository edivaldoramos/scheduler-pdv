package br.com.ithappens.pdv.scheduler.service;

import br.com.ithappens.pdv.scheduler.mapper.CargaClienteMapper;
import br.com.ithappens.pdv.scheduler.mapper.CargaMapper;
import br.com.ithappens.pdv.scheduler.model.Carga;
import br.com.ithappens.pdv.scheduler.model.dto.CargaClienteDTO;
import br.com.ithappens.pdv.scheduler.model.enums.TipoCarga;
import br.com.ithappens.pdv.scheduler.util.IWSMateus;
import com.opencsv.CSVWriter;
import com.opencsv.bean.ColumnPositionMappingStrategy;
import com.opencsv.bean.StatefulBeanToCsv;
import com.opencsv.bean.StatefulBeanToCsvBuilder;
import com.opencsv.exceptions.CsvDataTypeMismatchException;
import com.opencsv.exceptions.CsvRequiredFieldEmptyException;
import lombok.extern.slf4j.Slf4j;
import org.apache.commons.compress.archivers.sevenz.SevenZArchiveEntry;
import org.apache.commons.compress.archivers.sevenz.SevenZMethod;
import org.apache.commons.compress.archivers.sevenz.SevenZOutputFile;
import org.apache.commons.compress.utils.IOUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Propagation;
import org.springframework.transaction.annotation.Transactional;

import javax.validation.ValidationException;
import java.io.*;
import java.time.LocalDateTime;
import java.util.ArrayList;
import java.util.List;

@Slf4j
@Service
public class GeracaoCargaCliente extends GeradorArquivoCarga {

    private static final String[] CSV_HEADER = {"id", "razaoSocial", "nomeFantasia", "tipoPessoa", "cpfOuCnpj"};

    @Autowired
    private CargaClienteMapper cargaClienteMapper;

    @Autowired
    private CargaMapper cargaMapper;

    @Autowired
    private IValidacaoGeracaoCargaClienteService validacaoGeracaoCargaClienteService;

    @Autowired
    private IWSMateus iwsMateus;

    @Override
    void gerarArquivo() {
        FileWriter fileWriter = null;

        try {
            LocalDateTime dataRecuperarClientesParaGerarCarga = this.getDataRecuperarClientesParaGerarCarga();

            List<CargaClienteDTO> cargaClienteDTOs = new ArrayList<>();

            cargaClienteDTOs.addAll(cargaClienteMapper.recuperarClientesComDataAtualizacaoMaiorIgualA(dataRecuperarClientesParaGerarCarga));

            validacaoGeracaoCargaClienteService.validarCargaClientesDTOsParaImpostar(cargaClienteDTOs);

            if (log.isDebugEnabled())
                log.debug("Objetos cargaClienteDTO são válidos. Quantidade a importar: " + cargaClienteDTOs.size());

            fileWriter = new FileWriter(this.getCaminhoCompletoArquivoPlanilha());

            CSVWriter csvWriter = new CSVWriter(fileWriter, '|' , '"', CSVWriter.DEFAULT_ESCAPE_CHARACTER, CSVWriter.DEFAULT_LINE_END);
            csvWriter.writeNext(new String[]{"ID_CLIENTE", "RAZAO_SOCIAL", "NOME_FANTASIA", "TIPO_PESSOA", "CPF_CNPJ"});

            ColumnPositionMappingStrategy<CargaClienteDTO> mappingStrategy =
                    new ColumnPositionMappingStrategy<CargaClienteDTO>();

            mappingStrategy.setType(CargaClienteDTO.class);
            mappingStrategy.setColumnMapping(CSV_HEADER);

            StatefulBeanToCsv<CargaClienteDTO> cargaClienteDTOStatefulBeanToCsv = new StatefulBeanToCsvBuilder<CargaClienteDTO>(fileWriter)
                    .withMappingStrategy(mappingStrategy)
                    .withQuotechar('"')
                    .withSeparator('|')
                    .build();

            cargaClienteDTOStatefulBeanToCsv.write(cargaClienteDTOs);

            if (log.isDebugEnabled()) log.debug("Arquivo csv gerado com sucesso.");
        } catch (ValidationException | IOException | CsvRequiredFieldEmptyException | CsvDataTypeMismatchException e) {
            System.out.println(e.getMessage());
            e.printStackTrace();
        } finally {
            try {
                if (fileWriter != null)
                    fileWriter.close();
            } catch (IOException e) {
                System.out.println("Flushing/closing error!");
                e.printStackTrace();
            }
        }
    }

    private LocalDateTime getDataRecuperarClientesParaGerarCarga() {
        Carga ultimaCargaSolicitada = cargaMapper.recuperarUltimaCargaSolicitada(TipoCarga.CLIENTE);
        validacaoGeracaoCargaClienteService.validarUltimaCargaSolicitada(ultimaCargaSolicitada);
        Carga ultimaCargaConcluida = cargaMapper.recuperarUltimaCargaProcessada(TipoCarga.CLIENTE);
        return ultimaCargaSolicitada.getDataRecuperarClientesParaGerarCarga(ultimaCargaConcluida);
    }

    @Override
    void compactarArquivo() throws IOException {
        final File compactado = new File(this.getCaminhoCompletoArquivoCompactado());
        final File descompactado = new File(this.getCaminhoCompletoArquivoPlanilha());

        final SevenZOutputFile outArchive = new SevenZOutputFile(compactado);
        outArchive.setContentCompression(SevenZMethod.BZIP2);
        try {

            SevenZArchiveEntry entry = outArchive.createArchiveEntry(descompactado, descompactado.getName());
            outArchive.putArchiveEntry(entry);
            copy(descompactado, outArchive);
            outArchive.closeArchiveEntry();

        } finally {
            outArchive.close();
        }
    }

    private void copy(final File src, final SevenZOutputFile dst) throws IOException {
        FileInputStream fis = null;
        try {
            fis = new FileInputStream(src);
            final byte[] buffer = new byte[8 * 1024];
            int bytesRead;
            while ((bytesRead = fis.read(buffer)) >= 0) {
                dst.write(buffer, 0, bytesRead);
            }
        } finally {
            if (fis != null) {
                fis.close();
            }
        }
    }

    @Override
    String getHashArquivoEnviadoServidorRemoto() throws IOException {
        InputStream inputStream = new FileInputStream(this.getCaminhoCompletoArquivoCompactado());
        byte[] byteArray = IOUtils.toByteArray(inputStream);
        return iwsMateus.upload("pdv-carga-cliente", byteArray, this.getNomeArquivoCompactado());
    }

    @Override
    @Transactional(propagation = Propagation.REQUIRED, rollbackFor = Throwable.class)
    void vincularHashArquivoAoControlePdv(String hash, LocalDateTime dataInicioProcesso) {
        validacaoGeracaoCargaClienteService.validarHashRetornoEnvioServidorRemoto(hash);
        validacaoGeracaoCargaClienteService.validarDataInicioProcesso(dataInicioProcesso);
        Carga ultimaCargaSolicitada = cargaMapper.recuperarUltimaCargaSolicitada(TipoCarga.CLIENTE);
        validacaoGeracaoCargaClienteService.validarUltimaCargaSolicitada(ultimaCargaSolicitada);

        ultimaCargaSolicitada.setHash(hash);
        ultimaCargaSolicitada.setData(dataInicioProcesso);

        cargaMapper.atualizar(ultimaCargaSolicitada);
    }

    @Override
    String getNomeArquivo() {
        return new String("clientes");
    }
}