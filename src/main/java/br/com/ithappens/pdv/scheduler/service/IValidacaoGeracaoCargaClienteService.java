package br.com.ithappens.pdv.scheduler.service;

import br.com.ithappens.pdv.scheduler.model.Carga;
import br.com.ithappens.pdv.scheduler.model.dto.CargaClienteDTO;
import javax.validation.ValidationException;
import java.time.LocalDate;
import java.time.LocalDateTime;
import java.util.List;

public interface IValidacaoGeracaoCargaClienteService {

    void validarHashRetornoEnvioServidorRemoto(String hash);

    void validarDataInicioProcesso(LocalDateTime dataInicioProcesso);

    void validarUltimaCargaSolicitada(Carga ultimaCargaSolicitada);

    /**
     * Deve validarCargaClientesDTOsParaImpostar se o tipo de cobrança das finalizadoras pode vender balcão.
     *
     * @param cargaClienteDTOs A lista de DTOs de carga cliente que serão validados.
     * @throws ValidationException Se a lista de DTOs passados não respeita alguma das validações uma exceção de validação será lançada.
     * @author Edivaldo Ramos
     */
    void validarCargaClientesDTOsParaImpostar(List<CargaClienteDTO> cargaClienteDTOs) throws ValidationException;
}
