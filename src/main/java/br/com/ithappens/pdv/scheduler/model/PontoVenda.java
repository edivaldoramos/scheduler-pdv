package br.com.ithappens.pdv.scheduler.model;

import br.com.ithappens.pdv.scheduler.model.enums.StatusPontoVenda;
import lombok.Data;
import java.util.Date;

@Data
public class PontoVenda {

    private Long id;

    String descricao;

    Boolean ativo;

    StatusPontoVenda status;

    Filial filial;

    Boolean isMestre;

    Long memoriaLivre;

    Long numeroPDV;

    Date data;

    String ip;

    String mac;

    String espacoDisponivel;

    String identificadorHardware;

    String modeloHD;

    public PontoVenda(Long id){
        this.id = id;
    }

}
