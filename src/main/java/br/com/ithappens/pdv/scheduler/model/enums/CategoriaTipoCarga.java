package br.com.ithappens.pdv.scheduler.model.enums;

import java.util.EnumSet;
import java.util.HashMap;
import java.util.Map;

public enum CategoriaTipoCarga {

    NENHUM (0), PRODUTO (1), TEMPLATE (2), CADASTRO (3), PARAMETRO (4), APLICATIVO (5);

    private static Map<Integer, CategoriaTipoCarga> lookup;

    static {
        lookup = new HashMap<Integer, CategoriaTipoCarga>();
        EnumSet<CategoriaTipoCarga> enumSet = EnumSet.allOf(CategoriaTipoCarga.class);
        for (CategoriaTipoCarga CategoriaTipoCarga : enumSet) {
            lookup.put(CategoriaTipoCarga.codigo, CategoriaTipoCarga);
        }
    }

    public int codigo;

    private CategoriaTipoCarga(int codigo) {
        this.codigo = codigo;

    }

    public static CategoriaTipoCarga fromCodigo(int codigo) {
        if (lookup.containsKey(codigo)) {
            return lookup.get(codigo);
        }
        throw new IllegalArgumentException(String.format("Código do CategoriaTipoCarga inválido: %d", codigo));
    }

    public int getCodigo() {
        return codigo;
    }

}
