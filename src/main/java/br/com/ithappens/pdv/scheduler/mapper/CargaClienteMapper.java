package br.com.ithappens.pdv.scheduler.mapper;

import br.com.ithappens.pdv.scheduler.model.dto.CargaClienteDTO;
import org.apache.ibatis.annotations.Mapper;
import org.springframework.stereotype.Repository;
import java.time.LocalDateTime;
import java.util.List;

@Mapper
@Repository
public interface CargaClienteMapper {

    List<CargaClienteDTO> recuperarClientesComDataAtualizacaoMaiorIgualA(LocalDateTime data);
}
