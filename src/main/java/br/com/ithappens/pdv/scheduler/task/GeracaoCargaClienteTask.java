package br.com.ithappens.pdv.scheduler.task;

import br.com.ithappens.pdv.scheduler.service.GeradorArquivoCarga;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.scheduling.annotation.Scheduled;
import org.springframework.stereotype.Component;

import java.io.IOException;

@Component
public class GeracaoCargaClienteTask implements IGeracaoCargaClienteTask {

    private final long SEGUNDO = 1000;
    private final long MINUTO = SEGUNDO * 60;
    private final long HORA = MINUTO * 60;

    @Autowired
    GeradorArquivoCarga geradorArquivoCarga;

    @Scheduled(fixedDelay = HORA * 12)
    @Override
    public void gerarCarga() throws IOException {
        geradorArquivoCarga.gerarCarga();
    }

}
