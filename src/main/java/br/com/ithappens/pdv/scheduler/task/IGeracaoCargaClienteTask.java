package br.com.ithappens.pdv.scheduler.task;

import java.io.IOException;

public interface IGeracaoCargaClienteTask {

    void gerarCarga() throws IOException;

}
