package br.com.ithappens.pdv.scheduler.model.enums;

import java.util.EnumSet;
import java.util.HashMap;
import java.util.Map;

public enum TipoCarga {

    TIPO_TEMPLATE(1),
    USUARIO(3),
    PDV(4),
    TIPO_PRODUTO(2),
    FILIAL_INF_FISCAL(5),
    ESTADO(6),
    CIDADE(7),
    EMPRESA(8),
    FILIAL(9),
    EVENTO(10),
    FINALIZADORA(11),
    PERFIL(12),
    CONF_PERFIL(13),
    EVENTO_FILIAL_PARAM    (14),
    CONTR_ESTADO_TELA(15),
    CONFIG_PARCELAMENTO    (16),
    VERSAO_ITMARKET(17),
    PRECO_IMEDIATO(18),
    CLISITEF(19),
    CLIENTE(20);

    private static Map<Integer, TipoCarga> lookup;

    static {
        lookup = new HashMap<Integer, TipoCarga>();
        EnumSet<TipoCarga> enumSet = EnumSet.allOf(TipoCarga.class);
        for (TipoCarga TipoCarga : enumSet) {
            lookup.put(TipoCarga.codigo, TipoCarga);
        }
    }

    public int codigo;

    private TipoCarga(int codigo) {
        this.codigo = codigo;

    }

    public static TipoCarga fromCodigo(int codigo) {
        if (lookup.containsKey(codigo)) {
            return lookup.get(codigo);
        }
        throw new IllegalArgumentException(String.format("Código do TipoCarga inválido: %d", codigo));
    }

    public int getCodigo() {
        return codigo;
    }

}
